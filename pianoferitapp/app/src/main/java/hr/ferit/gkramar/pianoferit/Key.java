package hr.ferit.gkramar.pianoferit;

import android.graphics.drawable.Drawable;
import java.util.ArrayList;



class Key {
    private Drawable drawable;
    private Piano piano;
    private Piano.PianoKeyListener listener;
    private ArrayList<Finger> fingers = new ArrayList<>();
    private int id;

    private static final int ACTION_KEY_DOWN = 0;
    private static final int ACTION_KEY_UP = 1;

    Key(int id, Piano piano) {
        this.id = id;
        this.piano = piano;
    }

    Boolean isPressed() {
        return fingers.size() > 0;
    }

    void press(Finger finger) {
        this.fingers.add(finger);
        this.piano.invalidate();
        if (listener != null) {
            this.listener.keyPressed(id, Key.ACTION_KEY_DOWN);
        }
    }

    void depress(Finger finger) {
        this.fingers.remove(finger);
        if (!isPressed()) {
            this.piano.invalidate();
            if (listener != null) {
                this.listener.keyPressed(id, Key.ACTION_KEY_UP);
            }
        }
    }

    public int getId() {
        return id;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    void setPianoKeyListener(Piano.PianoKeyListener listener) {
        this.listener = listener;
    }
}
