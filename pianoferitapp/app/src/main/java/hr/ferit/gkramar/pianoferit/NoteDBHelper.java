package hr.ferit.gkramar.pianoferit;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;



class NoteDBHelper extends SQLiteOpenHelper {

    private static String DB_PATH = "/data/data/hr.ferit.gkramar.pianoferit/databases/";
    private static String DB_NAME = "PianoDB.db";
    private SQLiteDatabase myDataBase;
    private final Context myContext;

    NoteDBHelper(Context context) {
        super(context, DB_NAME, null, 5);
        this.myContext = context;
    }

    void crateDatabase() throws IOException {
        boolean vtVarMi = DoesDatabaseExist();
        if (!vtVarMi) {
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = myContext.getAssets().open(DB_NAME); // Opening local db as the input stream
        String outFileName = DB_PATH + DB_NAME;  // Path to the just created empty db
        OutputStream myOutput = new FileOutputStream(outFileName); // Opening the empty db as the output stream
        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private boolean DoesDatabaseExist() {
        SQLiteDatabase control;
        try {
            String myPath = DB_PATH + DB_NAME;
            control = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        } catch (SQLiteException e) {
            control = null;
        }
        if (control != null) {
            control.close();
        }
        return control != null;
    }

    void openDataBase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    public Cursor Sample_use_of_helper() {

        return myDataBase.query("TABLE_NAME", null, null, null, null, null, null);
    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    ArrayList<Note> getSong(String songID) {
        String song = null;
        String SELECT_SONG = "SELECT * FROM \"Songs\" WHERE \"song_name\"=\"" + songID + "\";";
        SQLiteDatabase readableDatabase = this.getReadableDatabase();
        Cursor cursor = readableDatabase.rawQuery(SELECT_SONG, null);
        if (cursor.moveToFirst()) {
            do {
                song = cursor.getString(1);//element on the 0th is _ID
            } while (cursor.moveToNext());
        }
        cursor.close();
        readableDatabase.close();
        return convertStringToArray(song);
    }

    private static ArrayList<Note> convertStringToArray(String str) {
        String[] array;
        ArrayList<Note> notes = new ArrayList<>();
        if (str != null) {
            String strSeparator = ",";
            array = str.split(strSeparator);

            for (String note : array) {
                notes.add(new Note(note));
            }
        }
        return notes;
    }
}
