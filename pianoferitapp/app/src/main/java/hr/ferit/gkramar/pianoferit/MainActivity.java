package hr.ferit.gkramar.pianoferit;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    Spinner spWaveChoice, spPlayMode;
    Piano piano;
    RecyclerView rvNote;
    NoteAdapter mNoteAdapter;
    DividerItemDecoration dividerItemDecoration;
    NoteDBHelper DBHelper = null;
    RelativeLayout rlUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final LinearLayout main = (LinearLayout) findViewById(R.id.keyboard_layout);
        piano = new Piano(this);
        main.addView(piano);

        //Preaparing RecyclerView for showing hints
        Context context = getApplicationContext();
        rvNote = (RecyclerView) findViewById(R.id.rvNote);
        mNoteAdapter = new NoteAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());

        rvNote.addItemDecoration(dividerItemDecoration);
        rvNote.setLayoutManager(layoutManager);
        rvNote.setAdapter(mNoteAdapter);

        //Relative Layout onTouch options
        rlUp = (RelativeLayout) findViewById(R.id.rlUp);
        rlUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {

                return true;//always return true to consume event
            }
        });

        //Preloading database
        DBHelper = new NoteDBHelper(getApplicationContext());
        try {
            DBHelper.crateDatabase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        DBHelper.openDataBase();

        //Preaparing and setting spinner for wave choise
        spWaveChoice = (Spinner) findViewById(R.id.spWaveShape);
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.WaveShape,
                        R.layout.spinner_item);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWaveChoice.setAdapter(staticAdapter);
        spWaveChoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                piano.setWave((String) spWaveChoice.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        //Preaparing and setting spinner for play mode
        this.spPlayMode = (Spinner) findViewById(R.id.spPlayMode);
        staticAdapter = ArrayAdapter.createFromResource(this, R.array.PlayMode,
                R.layout.spinner_item);
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPlayMode.setAdapter(staticAdapter);
        piano.setSpinnerPlayMode(spPlayMode);
        spPlayMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                piano.setPlayMode((String) spPlayMode.getSelectedItem());
                piano.setAdapter(null);
                piano.nextNoteToPlay = -1;
                piano.invalidate();
                mNoteAdapter.clear();
                if (!spPlayMode.getSelectedItem().equals("Free play")) {
                    piano.setAdapter(mNoteAdapter);
                    mNoteAdapter.addNotes(DBHelper.getSong((String) spPlayMode.getSelectedItem()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }
}
