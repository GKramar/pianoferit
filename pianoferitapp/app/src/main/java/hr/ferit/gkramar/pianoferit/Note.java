package hr.ferit.gkramar.pianoferit;

/**
 * Created by Gabrijela on 27.6.2017..
 */

public class Note {
    private String note;

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() { return note;}

    public Note(String note) {this.note = note;}
}
