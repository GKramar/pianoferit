package hr.ferit.gkramar.pianoferit;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Gabrijela on 27.6.2017..
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {

    private ArrayList<Note> mNote;

    NoteAdapter() {
        ArrayList<Note> notes = new ArrayList<>();
        notes.add(new Note("C2"));
        mNote = notes;
    }

    NoteAdapter(ArrayList<Note> tone) {
        mNote = tone;
    }

    ArrayList<Note> getmNote() {
        return mNote;
    }

    @Override
    public NoteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View noteView = inflater.inflate(R.layout.item_note, parent, false);
        return new ViewHolder(noteView);
    }

    @Override
    public void onBindViewHolder(NoteAdapter.ViewHolder holder, int position) {
        Note note = this.mNote.get(position);
        holder.tvNote.setText(note.getNote());
    }

    @Override
    public int getItemCount() {
        return this.mNote.size();
    }

    void clear() {
        int size = this.mNote.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) this.mNote.remove(0);
            this.notifyItemRangeRemoved(0, size);
        }
    }

    void remove(int position) {
        if (mNote.size() != 0) {
            mNote.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, mNote.size());
        }
    }

    void addNotes(ArrayList<Note> tones) {
        this.mNote.addAll(tones);
        this.notifyItemRangeInserted(0, mNote.size() - 1);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNote;
        ViewHolder(View itemView) {
            super(itemView);
            this.tvNote = (TextView) itemView.findViewById(R.id.tvNote);
        }
    }
}
