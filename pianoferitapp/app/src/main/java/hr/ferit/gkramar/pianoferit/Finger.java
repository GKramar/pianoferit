package hr.ferit.gkramar.pianoferit;

import java.util.ArrayList;


class Finger {
    private ArrayList<Key> keys = new ArrayList<>();

    Boolean isPressing(Key key) {
        return this.keys.contains(key);
    }

    void press(Key key) {
        if (this.isPressing(key)) {
            return;
        }
        key.press(this);
        this.keys.add(key);
    }

    void lift() {
        for (Key key : keys) {
            key.depress(this);
        }
        keys.clear();
    }
}
