package hr.ferit.gkramar.pianoferit;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class Piano extends View {
    private static final int defaultKeyCount = 24;
    private TreeMap<Integer, Key> keymap_white;
    private TreeMap<Integer, Key> keymap_black;
    private TreeMap<Integer, Finger> fingers;
    private int white_key_resource_id;
    private int black_key_resource_id;
    ArrayList<Integer> black_key_indexes = new ArrayList<>(Arrays.asList(1, 3, 6, 8, 10));
    SoundPool mSoundPool;
    boolean mLoaded = false; //Are the sounds loaded into SoundPool
    HashMap<Integer, Integer> mSoundMap = new HashMap<>();
    int streamID[] = new int[defaultKeyCount];
    private String wave = "Sine wave";
    private NoteAdapter mNoteAdapter = null;
    int nextNoteToPlay = -1;
    Spinner spPlayMode;


    public void setWave(String wave) {
        this.wave = wave;
    }

    public void setSpinnerPlayMode(Spinner sp) {
        this.spPlayMode = sp;
    }

    public void setAdapter(NoteAdapter adapter) {
        mNoteAdapter = adapter;
    }

    public void setPlayMode(String playmode) {
        if (!playmode.equals("Free play"))
            Toast.makeText(getContext(), "Tap on keyboard to begin", Toast.LENGTH_SHORT).show();
    }

    public Piano(Context context) {
        this(context, null);
    }

    public Piano(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Piano(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        int black_key_resource_id = R.drawable.key_black;
        int white_key_resource_id = R.drawable.key_white;
        int key_count = defaultKeyCount;

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PianoView, 0, 0);
            black_key_resource_id = a.getResourceId(R.styleable.PianoView_blackKeyDrawable, R.drawable.key_black);
            white_key_resource_id = a.getResourceId(R.styleable.PianoView_whiteKeyDrawable, R.drawable.key_white);
            key_count = a.getInt(R.styleable.PianoView_keyCount, defaultKeyCount);
            a.recycle();
        }
        initPiano(white_key_resource_id, black_key_resource_id, key_count);
    }

    private void initPiano(int white_key_resource_id, int black_key_resource_id, int key_count) {
        this.fingers = new TreeMap<>();
        this.white_key_resource_id = white_key_resource_id;
        this.black_key_resource_id = black_key_resource_id;
        this.keymap_white = new TreeMap<>();
        this.keymap_black = new TreeMap<>();
        if (!mLoaded) this.loadSounds();
        for (int i = 0; i < key_count; i++) {
            if (black_key_indexes.contains(i % 12)) {
                keymap_black.put(i, new Key(i, this));
            } else {
                keymap_white.put(i, new Key(i, this));
            }
        }
    }

    interface PianoKeyListener {
        void keyPressed(int id, int action);
    }

    private Drawable drawKey(Canvas canvas, Boolean pressed, boolean temp, int resource_id, int bounds_l, int bounds_t, int bounds_r, int bounds_b) throws Resources.NotFoundException, XmlPullParserException, IOException {
        Drawable key = Drawable.createFromXml(getResources(), getResources().getXml(resource_id));
        if (nextNoteToPlay == -1 && mNoteAdapter == null)
            key.setState(pressed ? new int[]{android.R.attr.state_pressed} : new int[]{-android.R.attr.state_pressed});
        else
            key.setState(temp ? new int[]{android.R.attr.state_selected} : new int[]{-android.R.attr.state_selected});
        key.setBounds(bounds_l, bounds_t, bounds_r, bounds_b);
        key.draw(canvas);
        return key;
    }

    public void draw(Canvas canvas) {
        int KEY_WIDTH = this.getWidth() / this.keymap_white.size();//Width for white keys
        KEY_WIDTH = KEY_WIDTH + 1;
        int KEY_HEIGHT = this.getHeight();//Width for white keys
        int counter = 0;
        boolean setToPlay = false;
        try {
            for (Map.Entry<Integer, Key> key : this.keymap_white.entrySet()) {
                if (nextNoteToPlay != -1) {
                    if (nextNoteToPlay == 0 && counter == 0) setToPlay = true;
                    else if (nextNoteToPlay == 2 && counter == 1) setToPlay = true;
                    else if (nextNoteToPlay == 4 && counter == 2) setToPlay = true;
                    else if (nextNoteToPlay == 5 && counter == 3) setToPlay = true;
                    else if (nextNoteToPlay == 7 && counter == 4) setToPlay = true;
                    else if (nextNoteToPlay == 9 && counter == 5) setToPlay = true;
                    else if (nextNoteToPlay == 11 && counter == 6) setToPlay = true;
                    else if (nextNoteToPlay == 12 && counter == 7) setToPlay = true;
                    else if (nextNoteToPlay == 14 && counter == 8) setToPlay = true;
                    else if (nextNoteToPlay == 16 && counter == 9) setToPlay = true;
                    else if (nextNoteToPlay == 17 && counter == 10) setToPlay = true;
                    else if (nextNoteToPlay == 19 && counter == 11) setToPlay = true;
                    else if (nextNoteToPlay == 21 && counter == 12) setToPlay = true;
                    else if (nextNoteToPlay == 23 && counter == 13) setToPlay = true;
                    else setToPlay = false;
                }
                int bounds_left = counter * KEY_WIDTH;
                int bounds_top = 0;
                int bounds_right = (counter * KEY_WIDTH) + KEY_WIDTH;
                Drawable white_key = this.drawKey(canvas, key.getValue().isPressed(), setToPlay, white_key_resource_id, bounds_left, bounds_top, bounds_right, KEY_HEIGHT);
                key.getValue().setDrawable(white_key);
                counter++;
            }
            counter = 0;
            setToPlay = false;
            for (Map.Entry<Integer, Key> key : keymap_black.entrySet()) {
                if (((counter - 2) % 7 == 0) || ((counter - 6) % 7 == 0)) {
                    counter++;
                }
                if (nextNoteToPlay != -1) {
                    if (nextNoteToPlay == 1 && counter == 0) setToPlay = true;
                    else if (nextNoteToPlay == 3 && counter == 1) setToPlay = true;
                    else if (nextNoteToPlay == 6 && counter == 3) setToPlay = true;
                    else if (nextNoteToPlay == 8 && counter == 4) setToPlay = true;
                    else if (nextNoteToPlay == 10 && counter == 5) setToPlay = true;
                    else if (nextNoteToPlay == 13 && counter == 7) setToPlay = true;
                    else if (nextNoteToPlay == 15 && counter == 8) setToPlay = true;
                    else if (nextNoteToPlay == 18 && counter == 10) setToPlay = true;
                    else if (nextNoteToPlay == 20 && counter == 11) setToPlay = true;
                    else if (nextNoteToPlay == 22 && counter == 12) setToPlay = true;
                    else setToPlay = false;
                }
                int bounds_left = (counter * KEY_WIDTH) + (2 * KEY_WIDTH / 3);
                int bounds_top = 0;
                int bounds_right = (counter * KEY_WIDTH) + (2 * KEY_WIDTH / 3 + (2 * KEY_WIDTH / 3));
                int bounds_bottom = KEY_HEIGHT / 2;
                Drawable black_key = this.drawKey(canvas, key.getValue().isPressed(), setToPlay, black_key_resource_id, bounds_left, bounds_top, bounds_right, bounds_bottom);
                key.getValue().setDrawable(black_key);
                counter++;
            }
            this.setOnTouchListener(new KeyPressListener(fingers, keymap_white, keymap_black));

        } catch (Exception e) {
            Toast.makeText(this.getContext(), "Error drawing keys", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private class KeyPressListener implements OnTouchListener {
        private TreeMap<Integer, Finger> fingers;
        private TreeMap<Integer, Key> keymap_white;
        private TreeMap<Integer, Key> keymap_black;

        KeyPressListener(TreeMap<Integer, Finger> fingers, TreeMap<Integer, Key> keymap, TreeMap<Integer, Key> blackkeymap) {
            this.fingers = fingers;
            this.keymap_white = keymap;
            this.keymap_black = blackkeymap;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getActionMasked();

            switch (action) {
                case MotionEvent.ACTION_MOVE:
                    handleActionPointerMove(event);
                    break;
                case MotionEvent.ACTION_DOWN:
                    handleActionDown(event);
                    break;
                case MotionEvent.ACTION_UP:
                    handleActionUp(event);
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    handleActionDown(event);
                    break;
                case MotionEvent.ACTION_POINTER_UP:
                    handleActionUp(event);
                    break;
            }
            return true;
        }

        void handleActionPointerMove(MotionEvent event) {
            for (int i = 0; i < event.getPointerCount(); i++) {
                handlePointerIndex(i, event);
            }
        }

        void handleActionUp(MotionEvent event) {
            //Changing spinner value when the last note was payed and let go of
            if (mNoteAdapter == null && nextNoteToPlay != -1) {
                spPlayMode.setSelection(0);
            }

            final int action = event.getAction();
            final int pointerindex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK)
                    >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
            int pointer_index = event.getPointerId(event.getActionIndex());
            Key key = isPressingKey(event.getX(pointerindex), event.getY(pointerindex));
            if (key != null) {
                mSoundPool.stop(streamID[key.getId()]);
                fingers.get(pointer_index).lift();
                fingers.remove(pointer_index);
            } else {
                for (int i = 0; i < defaultKeyCount; i++) mSoundPool.stop(streamID[i]);
                fingers.get(pointer_index).lift();
                fingers.remove(pointer_index);
            }
        }

        private void handleActionDown(MotionEvent event) {
            if (mNoteAdapter != null) {
                setNextNoteToPlay();
            }
            final int action = event.getAction();
            final int pointerindex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK)
                    >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
            int pointer_index = event.getPointerId(event.getActionIndex());
            Key key = isPressingKey(event.getX(pointerindex), event.getY(pointerindex));
            if (key != null) {
                if (!fingers.containsKey(pointer_index)) {
                    Finger finger = new Finger();
                    fingers.put(pointer_index, finger);
                    finger.press(key);
                }
                if (mNoteAdapter != null) {
                    if (nextNoteToPlay == key.getId())
                        mNoteAdapter.remove(0);
                }
                chooseSound(key.getId());
            }
        }

        private void handlePointerIndex(int index, MotionEvent event) {
            if (mNoteAdapter != null) {
                setNextNoteToPlay();
            }
            int pointer_id = event.getPointerId(index);
            //Has it moved off a key?
            Key key = isPressingKey(event.getX(index), event.getY(index));
            Finger finger = fingers.get(pointer_id);
            if (key == null) {
                for (int i = 0; i < defaultKeyCount; i++) mSoundPool.stop(streamID[i]);
                finger.lift();
            } else if (!finger.isPressing(key)) {
                finger.lift();
                for (int i = 0; i < defaultKeyCount; i++) mSoundPool.stop(streamID[i]);
                finger.press(key);
                chooseSound(key.getId());
                if (mNoteAdapter != null && nextNoteToPlay == key.getId())
                    mNoteAdapter.remove(0);
            }
        }

        private Key isPressingKey(float xpos, float ypos) {
            Key pressing_key;
            if ((pressing_key = isPressingKeyInSet(xpos, ypos, keymap_black)) != null) {
                return pressing_key;
            } else {
                return isPressingKeyInSet(xpos, ypos, keymap_white);
            }
        }

        private Key isPressingKeyInSet(float xpos, float ypos, TreeMap<Integer, Key> keyset) {
            for (Map.Entry<Integer, Key> entry : keyset.entrySet()) {
                if (entry.getValue() != null && entry.getValue().getDrawable() != null && entry.getValue().getDrawable().getBounds().contains((int) xpos, (int) ypos)) {
                    return entry.getValue();
                }
            }
            return null;
        }
    }

    private void chooseSound(int id) {
        switch (wave) {
            case "Sine wave":
                if (id == 0) playSound(R.raw.c3sine, id);
                else if (id == 1) playSound(R.raw.c3sharpsine, id);
                else if (id == 2) playSound(R.raw.d3sine, id);
                else if (id == 3) playSound(R.raw.d3sharpsine, id);
                else if (id == 4) playSound(R.raw.e3sine, id);
                else if (id == 5) playSound(R.raw.f3sine, id);
                else if (id == 6) playSound(R.raw.f3sharpsine, id);
                else if (id == 7) playSound(R.raw.g3sine, id);
                else if (id == 8) playSound(R.raw.g3sharpsine, id);
                else if (id == 9) playSound(R.raw.a3sine, id);
                else if (id == 10) playSound(R.raw.a3sharpsine, id);
                else if (id == 11) playSound(R.raw.h3sine, id);
                else if (id == 12) playSound(R.raw.c4sine, id);
                else if (id == 13) playSound(R.raw.c4sharpsine, id);
                else if (id == 14) playSound(R.raw.d4sine, id);
                else if (id == 15) playSound(R.raw.d4sharpsine, id);
                else if (id == 16) playSound(R.raw.e4sine, id);
                else if (id == 17) playSound(R.raw.f4sine, id);
                else if (id == 18) playSound(R.raw.f4sharpsine, id);
                else if (id == 19) playSound(R.raw.g4sine, id);
                else if (id == 20) playSound(R.raw.g4sharpsine, id);
                else if (id == 21) playSound(R.raw.a4sine, id);
                else if (id == 22) playSound(R.raw.a4sharpsine, id);
                else playSound(R.raw.h4sine, id);
                break;
            case "Square wave":
                if (id == 0) playSound(R.raw.c3square, id);
                else if (id == 1) playSound(R.raw.c3sharpsquare, id);
                else if (id == 2) playSound(R.raw.d3square, id);
                else if (id == 3) playSound(R.raw.d3sharpsquare, id);
                else if (id == 4) playSound(R.raw.e3square, id);
                else if (id == 5) playSound(R.raw.f3square, id);
                else if (id == 6) playSound(R.raw.f3sharpsquare, id);
                else if (id == 7) playSound(R.raw.g3square, id);
                else if (id == 8) playSound(R.raw.g3sharpsquare, id);
                else if (id == 9) playSound(R.raw.a3square, id);
                else if (id == 10) playSound(R.raw.a3sharpsquare, id);
                else if (id == 11) playSound(R.raw.h3square, id);
                else if (id == 12) playSound(R.raw.c4square, id);
                else if (id == 13) playSound(R.raw.c4sharpsquare, id);
                else if (id == 14) playSound(R.raw.d4square, id);
                else if (id == 15) playSound(R.raw.d4sharpsquare, id);
                else if (id == 16) playSound(R.raw.e4square, id);
                else if (id == 17) playSound(R.raw.f4square, id);
                else if (id == 18) playSound(R.raw.f4sharpsquare, id);
                else if (id == 19) playSound(R.raw.g4square, id);
                else if (id == 20) playSound(R.raw.g4sharpsquare, id);
                else if (id == 21) playSound(R.raw.a4square, id);
                else if (id == 22) playSound(R.raw.a4sharpsquare, id);
                else playSound(R.raw.h4square, id);
                break;
            case "Sawtooth wave":
                if (id == 0) playSound(R.raw.c3sawtooth, id);
                else if (id == 1) playSound(R.raw.c3sharpsawtooth, id);
                else if (id == 2) playSound(R.raw.d3sawtooth, id);
                else if (id == 3) playSound(R.raw.d3sharpsawtooth, id);
                else if (id == 4) playSound(R.raw.e3sawtooth, id);
                else if (id == 5) playSound(R.raw.f3sawtooth, id);
                else if (id == 6) playSound(R.raw.f3sharpsawtooth, id);
                else if (id == 7) playSound(R.raw.g3sawtooth, id);
                else if (id == 8) playSound(R.raw.g3sharpsawtooth, id);
                else if (id == 9) playSound(R.raw.a3sawtooth, id);
                else if (id == 10) playSound(R.raw.a3sharpsawtooth, id);
                else if (id == 11) playSound(R.raw.h3sawtooth, id);
                else if (id == 12) playSound(R.raw.c4sawtooth, id);
                else if (id == 13) playSound(R.raw.c4sharpsawtooth, id);
                else if (id == 14) playSound(R.raw.d4sawtooth, id);
                else if (id == 15) playSound(R.raw.d4sharpsawtooth, id);
                else if (id == 16) playSound(R.raw.e4sawtooth, id);
                else if (id == 17) playSound(R.raw.f4sawtooth, id);
                else if (id == 18) playSound(R.raw.f4sharpsawtooth, id);
                else if (id == 19) playSound(R.raw.g4sawtooth, id);
                else if (id == 20) playSound(R.raw.g4sharpsawtooth, id);
                else if (id == 21) playSound(R.raw.a4sawtooth, id);
                else if (id == 22) playSound(R.raw.a4sharpsawtooth, id);
                else playSound(R.raw.h4sawtooth, id);
                break;
            case "Triangle wave":
                if (id == 0) playSound(R.raw.c3triangle, id);
                else if (id == 1) playSound(R.raw.c3sharptriangle, id);
                else if (id == 2) playSound(R.raw.d3triangle, id);
                else if (id == 3) playSound(R.raw.d3sharptriangle, id);
                else if (id == 4) playSound(R.raw.e3triangle, id);
                else if (id == 5) playSound(R.raw.f3triangle, id);
                else if (id == 6) playSound(R.raw.f3sharptriangle, id);
                else if (id == 7) playSound(R.raw.g3triangle, id);
                else if (id == 8) playSound(R.raw.g3sharptriangle, id);
                else if (id == 9) playSound(R.raw.a3triangle, id);
                else if (id == 10) playSound(R.raw.a3sharptriangle, id);
                else if (id == 11) playSound(R.raw.h3triangle, id);
                else if (id == 12) playSound(R.raw.c4triangle, id);
                else if (id == 13) playSound(R.raw.c4sharptriangle, id);
                else if (id == 14) playSound(R.raw.d4triangle, id);
                else if (id == 15) playSound(R.raw.d4sharptriangle, id);
                else if (id == 16) playSound(R.raw.e4triangle, id);
                else if (id == 17) playSound(R.raw.f4triangle, id);
                else if (id == 18) playSound(R.raw.f4sharptriangle, id);
                else if (id == 19) playSound(R.raw.g4triangle, id);
                else if (id == 20) playSound(R.raw.g4sharptriangle, id);
                else if (id == 21) playSound(R.raw.a4triangle, id);
                else if (id == 22) playSound(R.raw.a4sharptriangle, id);
                else playSound(R.raw.h4triangle, id);
                break;
        }
    }

    private void loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = new SoundPool.Builder().setMaxStreams(10).build();
        } else {
            this.mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }
        this.mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Log.d("Loaded", String.valueOf(sampleId));
                mLoaded = true;
            }
        });
        //Sine wave
        this.mSoundMap.put(R.raw.c3sine, this.mSoundPool.load(getContext(), R.raw.c3sine, 1));
        this.mSoundMap.put(R.raw.c3sharpsine, this.mSoundPool.load(getContext(), R.raw.c3sharpsine, 1));
        this.mSoundMap.put(R.raw.d3sine, this.mSoundPool.load(getContext(), R.raw.d3sine, 1));
        this.mSoundMap.put(R.raw.d3sharpsine, this.mSoundPool.load(getContext(), R.raw.d3sharpsine, 1));
        this.mSoundMap.put(R.raw.e3sine, this.mSoundPool.load(getContext(), R.raw.e3sine, 1));
        this.mSoundMap.put(R.raw.f3sine, this.mSoundPool.load(getContext(), R.raw.f3sine, 1));
        this.mSoundMap.put(R.raw.f3sharpsine, this.mSoundPool.load(getContext(), R.raw.f3sharpsine, 1));
        this.mSoundMap.put(R.raw.g3sine, this.mSoundPool.load(getContext(), R.raw.g3sine, 1));
        this.mSoundMap.put(R.raw.g3sharpsine, this.mSoundPool.load(getContext(), R.raw.g3sharpsine, 1));
        this.mSoundMap.put(R.raw.a3sine, this.mSoundPool.load(getContext(), R.raw.a3sine, 1));
        this.mSoundMap.put(R.raw.a3sharpsine, this.mSoundPool.load(getContext(), R.raw.a3sharpsine, 1));
        this.mSoundMap.put(R.raw.h3sine, this.mSoundPool.load(getContext(), R.raw.h3sine, 1));
        this.mSoundMap.put(R.raw.c4sine, this.mSoundPool.load(getContext(), R.raw.c4sine, 1));
        this.mSoundMap.put(R.raw.c4sharpsine, this.mSoundPool.load(getContext(), R.raw.c4sharpsine, 1));
        this.mSoundMap.put(R.raw.d4sine, this.mSoundPool.load(getContext(), R.raw.d4sine, 1));
        this.mSoundMap.put(R.raw.d4sharpsine, this.mSoundPool.load(getContext(), R.raw.d4sharpsine, 1));
        this.mSoundMap.put(R.raw.e4sine, this.mSoundPool.load(getContext(), R.raw.e4sine, 1));
        this.mSoundMap.put(R.raw.f4sine, this.mSoundPool.load(getContext(), R.raw.f4sine, 1));
        this.mSoundMap.put(R.raw.f4sharpsine, this.mSoundPool.load(getContext(), R.raw.f4sharpsine, 1));
        this.mSoundMap.put(R.raw.g4sine, this.mSoundPool.load(getContext(), R.raw.g4sine, 1));
        this.mSoundMap.put(R.raw.g4sharpsine, this.mSoundPool.load(getContext(), R.raw.g4sharpsine, 1));
        this.mSoundMap.put(R.raw.a4sine, this.mSoundPool.load(getContext(), R.raw.a4sine, 1));
        this.mSoundMap.put(R.raw.a4sharpsine, this.mSoundPool.load(getContext(), R.raw.a4sharpsine, 1));
        this.mSoundMap.put(R.raw.h4sine, this.mSoundPool.load(getContext(), R.raw.h4sine, 1));
        //Square wave
        this.mSoundMap.put(R.raw.c3square, this.mSoundPool.load(getContext(), R.raw.c3square, 1));
        this.mSoundMap.put(R.raw.c3sharpsquare, this.mSoundPool.load(getContext(), R.raw.c3sharpsquare, 1));
        this.mSoundMap.put(R.raw.d3square, this.mSoundPool.load(getContext(), R.raw.d3square, 1));
        this.mSoundMap.put(R.raw.d3sharpsquare, this.mSoundPool.load(getContext(), R.raw.d3sharpsquare, 1));
        this.mSoundMap.put(R.raw.e3square, this.mSoundPool.load(getContext(), R.raw.e3square, 1));
        this.mSoundMap.put(R.raw.f3square, this.mSoundPool.load(getContext(), R.raw.f3square, 1));
        this.mSoundMap.put(R.raw.f3sharpsquare, this.mSoundPool.load(getContext(), R.raw.f3sharpsquare, 1));
        this.mSoundMap.put(R.raw.g3square, this.mSoundPool.load(getContext(), R.raw.g3square, 1));
        this.mSoundMap.put(R.raw.g3sharpsquare, this.mSoundPool.load(getContext(), R.raw.g3sharpsquare, 1));
        this.mSoundMap.put(R.raw.a3square, this.mSoundPool.load(getContext(), R.raw.a3square, 1));
        this.mSoundMap.put(R.raw.a3sharpsquare, this.mSoundPool.load(getContext(), R.raw.a3sharpsquare, 1));
        this.mSoundMap.put(R.raw.h3square, this.mSoundPool.load(getContext(), R.raw.h3square, 1));
        this.mSoundMap.put(R.raw.c4square, this.mSoundPool.load(getContext(), R.raw.c4square, 1));
        this.mSoundMap.put(R.raw.c4sharpsquare, this.mSoundPool.load(getContext(), R.raw.c4sharpsquare, 1));
        this.mSoundMap.put(R.raw.d4square, this.mSoundPool.load(getContext(), R.raw.d4square, 1));
        this.mSoundMap.put(R.raw.d4sharpsquare, this.mSoundPool.load(getContext(), R.raw.d4sharpsquare, 1));
        this.mSoundMap.put(R.raw.e4square, this.mSoundPool.load(getContext(), R.raw.e4square, 1));
        this.mSoundMap.put(R.raw.f4square, this.mSoundPool.load(getContext(), R.raw.f4square, 1));
        this.mSoundMap.put(R.raw.f4sharpsquare, this.mSoundPool.load(getContext(), R.raw.f4sharpsquare, 1));
        this.mSoundMap.put(R.raw.g4square, this.mSoundPool.load(getContext(), R.raw.g4square, 1));
        this.mSoundMap.put(R.raw.g4sharpsquare, this.mSoundPool.load(getContext(), R.raw.g4sharpsquare, 1));
        this.mSoundMap.put(R.raw.a4square, this.mSoundPool.load(getContext(), R.raw.a4square, 1));
        this.mSoundMap.put(R.raw.a4sharpsquare, this.mSoundPool.load(getContext(), R.raw.a4sharpsquare, 1));
        this.mSoundMap.put(R.raw.h4square, this.mSoundPool.load(getContext(), R.raw.h4square, 1));
        //Sawtooth wave
        this.mSoundMap.put(R.raw.c3sawtooth, this.mSoundPool.load(getContext(), R.raw.c3sawtooth, 1));
        this.mSoundMap.put(R.raw.c3sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.c3sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.d3sawtooth, this.mSoundPool.load(getContext(), R.raw.d3sawtooth, 1));
        this.mSoundMap.put(R.raw.d3sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.d3sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.e3sawtooth, this.mSoundPool.load(getContext(), R.raw.e3sawtooth, 1));
        this.mSoundMap.put(R.raw.f3sawtooth, this.mSoundPool.load(getContext(), R.raw.f3sawtooth, 1));
        this.mSoundMap.put(R.raw.f3sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.f3sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.g3sawtooth, this.mSoundPool.load(getContext(), R.raw.g3sawtooth, 1));
        this.mSoundMap.put(R.raw.g3sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.g3sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.a3sawtooth, this.mSoundPool.load(getContext(), R.raw.a3sawtooth, 1));
        this.mSoundMap.put(R.raw.a3sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.a3sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.h3sawtooth, this.mSoundPool.load(getContext(), R.raw.h3sawtooth, 1));
        this.mSoundMap.put(R.raw.c4sawtooth, this.mSoundPool.load(getContext(), R.raw.c4sawtooth, 1));
        this.mSoundMap.put(R.raw.c4sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.c4sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.d4sawtooth, this.mSoundPool.load(getContext(), R.raw.d4sawtooth, 1));
        this.mSoundMap.put(R.raw.d4sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.d4sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.e4sawtooth, this.mSoundPool.load(getContext(), R.raw.e4sawtooth, 1));
        this.mSoundMap.put(R.raw.f4sawtooth, this.mSoundPool.load(getContext(), R.raw.f4sawtooth, 1));
        this.mSoundMap.put(R.raw.f4sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.f4sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.g4sawtooth, this.mSoundPool.load(getContext(), R.raw.g4sawtooth, 1));
        this.mSoundMap.put(R.raw.g4sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.g4sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.a4sawtooth, this.mSoundPool.load(getContext(), R.raw.a4sawtooth, 1));
        this.mSoundMap.put(R.raw.a4sharpsawtooth, this.mSoundPool.load(getContext(), R.raw.a4sharpsawtooth, 1));
        this.mSoundMap.put(R.raw.h4sawtooth, this.mSoundPool.load(getContext(), R.raw.h4sawtooth, 1));
        //Trinagle wave
        this.mSoundMap.put(R.raw.c3triangle, this.mSoundPool.load(getContext(), R.raw.c3triangle, 1));
        this.mSoundMap.put(R.raw.c3sharptriangle, this.mSoundPool.load(getContext(), R.raw.c3sharptriangle, 1));
        this.mSoundMap.put(R.raw.d3triangle, this.mSoundPool.load(getContext(), R.raw.d3triangle, 1));
        this.mSoundMap.put(R.raw.d3sharptriangle, this.mSoundPool.load(getContext(), R.raw.d3sharptriangle, 1));
        this.mSoundMap.put(R.raw.e3triangle, this.mSoundPool.load(getContext(), R.raw.e3triangle, 1));
        this.mSoundMap.put(R.raw.f3triangle, this.mSoundPool.load(getContext(), R.raw.f3triangle, 1));
        this.mSoundMap.put(R.raw.f3sharptriangle, this.mSoundPool.load(getContext(), R.raw.f3sharptriangle, 1));
        this.mSoundMap.put(R.raw.g3triangle, this.mSoundPool.load(getContext(), R.raw.g3triangle, 1));
        this.mSoundMap.put(R.raw.g3sharptriangle, this.mSoundPool.load(getContext(), R.raw.g3sharptriangle, 1));
        this.mSoundMap.put(R.raw.a3triangle, this.mSoundPool.load(getContext(), R.raw.a3triangle, 1));
        this.mSoundMap.put(R.raw.a3sharptriangle, this.mSoundPool.load(getContext(), R.raw.a3sharptriangle, 1));
        this.mSoundMap.put(R.raw.h3triangle, this.mSoundPool.load(getContext(), R.raw.h3triangle, 1));
        this.mSoundMap.put(R.raw.c4triangle, this.mSoundPool.load(getContext(), R.raw.c4triangle, 1));
        this.mSoundMap.put(R.raw.c4sharptriangle, this.mSoundPool.load(getContext(), R.raw.c4sharptriangle, 1));
        this.mSoundMap.put(R.raw.d4triangle, this.mSoundPool.load(getContext(), R.raw.d4triangle, 1));
        this.mSoundMap.put(R.raw.d4sharptriangle, this.mSoundPool.load(getContext(), R.raw.d4sharptriangle, 1));
        this.mSoundMap.put(R.raw.e4triangle, this.mSoundPool.load(getContext(), R.raw.e4triangle, 1));
        this.mSoundMap.put(R.raw.f4triangle, this.mSoundPool.load(getContext(), R.raw.f4triangle, 1));
        this.mSoundMap.put(R.raw.f4sharptriangle, this.mSoundPool.load(getContext(), R.raw.f4sharptriangle, 1));
        this.mSoundMap.put(R.raw.g4triangle, this.mSoundPool.load(getContext(), R.raw.g4triangle, 1));
        this.mSoundMap.put(R.raw.g4sharptriangle, this.mSoundPool.load(getContext(), R.raw.g4sharptriangle, 1));
        this.mSoundMap.put(R.raw.a4triangle, this.mSoundPool.load(getContext(), R.raw.a4triangle, 1));
        this.mSoundMap.put(R.raw.a4sharptriangle, this.mSoundPool.load(getContext(), R.raw.a4sharptriangle, 1));
        this.mSoundMap.put(R.raw.h4triangle, this.mSoundPool.load(getContext(), R.raw.h4triangle, 1));
    }

    void playSound(int selectedSound, int id) {
        int soundID = this.mSoundMap.get(selectedSound);
        streamID[id] = this.mSoundPool.play(soundID, 1, 1, 1, -1, 1);
    }

    public void setNextNoteToPlay() {
        if (mNoteAdapter != null) {
            if (mNoteAdapter.getItemCount() != 0) {
                switch (mNoteAdapter.getmNote().get(0).getNote()) {
                    case "C3":
                        nextNoteToPlay = 0;
                        break;
                    case "C3#":
                        nextNoteToPlay = 1;
                        break;
                    case "D3":
                        nextNoteToPlay = 2;
                        break;
                    case "D3#":
                        nextNoteToPlay = 3;
                        break;
                    case "E3":
                        nextNoteToPlay = 4;
                        break;
                    case "F3":
                        nextNoteToPlay = 5;
                        break;
                    case "F3#":
                        nextNoteToPlay = 6;
                        break;
                    case "G3":
                        nextNoteToPlay = 7;
                        break;
                    case "G3#":
                        nextNoteToPlay = 8;
                        break;
                    case "A3":
                        nextNoteToPlay = 9;
                        break;
                    case "A3#":
                        nextNoteToPlay = 10;
                        break;
                    case "H3":
                        nextNoteToPlay = 11;
                        break;
                    case "C4":
                        nextNoteToPlay = 12;
                        break;
                    case "C4#":
                        nextNoteToPlay = 13;
                        break;
                    case "D4":
                        nextNoteToPlay = 14;
                        break;
                    case "D4#":
                        nextNoteToPlay = 15;
                        break;
                    case "E4":
                        nextNoteToPlay = 16;
                        break;
                    case "F4":
                        nextNoteToPlay = 17;
                        break;
                    case "F4#":
                        nextNoteToPlay = 18;
                        break;
                    case "G4":
                        nextNoteToPlay = 19;
                        break;
                    case "G4#":
                        nextNoteToPlay = 20;
                        break;
                    case "A4":
                        nextNoteToPlay = 21;
                        break;
                    case "A4#":
                        nextNoteToPlay = 22;
                        break;
                    case "H4":
                        nextNoteToPlay = 23;
                        break;
                }
            } else {
                mNoteAdapter = null;
            }
        }
    }
}
